#!/bin/bash

mmc_is_valid()
{
    if [ "${1:-}" ]; then
        BLK_DEV=$1
    fi

    if ! [[ -a "${BLK_DEV}" ]]; then
        return 1
    fi
}

print_help()
{
    if [ ! -z ${1+x} ]; then
        echo "$1"
    fi

    echo ""
    echo "$ ./`basename $0` [option]"
    echo ""
    echo "Actions:"
    echo "-b [device]      -- Build everything and create bootable image on device"
    echo ""

    if [ ! -z ${2+x} ]; then
        echo "$2"
    fi

    exit 1
}

parse_args()
{
    while getopts ":kab:d:" opt;
    do
    case "$opt" in
        b)
            mmc_is_valid $OPTARG || print_help "Invalid MMC supplied!"
            BLK_DEV=$OPTARG
            ;;
        *)
            echo "*)"
            print_help "Invalid argument -$opt $OPTARG"
            ;;
        esac
    done
    shift $((OPTIND-1))
}

ROOTDIR="`dirname "$0"`/.."
ROOTDIR="`realpath "$ROOTDIR"`"
cd $ROOTDIR

parse_args $@

mkdir -p debos/overlays/spurv/boot
echo "Adding RDU2 Support"
cp linux/arch/arm/boot/dts/imx6qp-zii-rdu2.dtb debos/overlays/spurv/boot/.
echo "Adding Nitrogen6QP_MAX Support"
cp linux/arch/arm/boot/dts/imx6qp-nitrogen6_max.dtb debos/overlays/spurv/boot/.
mkimage -A arm -O linux -T script -C none -n boot.scr -d debos/uboot_nitrogen6qp-max.scr debos/overlays/spurv/boot/boot.scr &> /dev/null

echo "Adding Linux Kernel"
cp linux/arch/arm/boot/zImage debos/overlays/spurv/boot/.

echo "Adding Android image"
mkdir -p debos/overlays/spurv/home/aosp
cp android/device/freedesktop/spurv/launch-container.sh debos/overlays/spurv/home/aosp/.
cp android/device/freedesktop/spurv/run.sh debos/overlays/spurv/home/aosp/.
cp android/out/target/product/spurv/system.img debos/overlays/spurv/home/aosp/aosp.img
cp android/out/target/product/spurv/vendor.img debos/overlays/spurv/home/aosp/.
cp android/out/target/product/spurv/userdata.img debos/overlays/spurv/home/aosp/.

cd $ROOTDIR/debos
sudo chmod 0666 /dev/kvm
docker run \
    --rm \
    --interactive \
    --tty \
    --device /dev/kvm \
    --user $(id -u) \
    --workdir /recipes \
    --mount "type=bind,source=$(pwd),destination=/recipes" \
    --security-opt label=disable \
        godebos/debos \
            --scratchsize 4G \
            spurv.yaml \
    || exit 0
cd - &> /dev/null

if [ ! -z ${BLK_DEV+x} ]; then
    umount $BLK_DEV* &> /dev/null
    sudo bmaptool copy --bmap debos/aosp_demo-testing-armhf.img.bmap debos/aosp_demo-testing-armhf.img $BLK_DEV
    sync
fi